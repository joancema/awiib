const express= require("express");
const app =  express();

const response={
    data:[],
    services:"Cars services",
    architecture:"Microservices"
}
const logger = message => console.log("Car Service:"+ message);

app.use((req,res,next)=>{
    response.data=[];
    next();
})

app.get("/api/v2/cars", (req,res)=>{
    response.data.push("Volvo", "Kia rio", "WV Vento");
    logger("Get Car Data")
    return res.send(response);
})

module.exports = app;