const express = require("express");
const app = express();

const response = {
    data:[],
    services: "All services",
    architecture:"Monolithic"
}

app.use((req,res,next)=>{
    response.data=[];
    next();
})

//libros
app.get("/api/v1/books",(req,res)=>{
    response.data.push("JavaScript para principiantes", "JS de 0 a experto", "JS React");
    return res.send(response);
})
//vehiculos
app.get("/api/v1/cars",(req,res)=>{
    response.data.push("Fiat1", "Volvo", "WV");
    return res.send(response);
})
//usuarios
app.get("/api/v1/users", (req,res)=>{
    response.data.push("admin", "invitado", "sextob");
    return res.send(response);
})

module.exports = app;